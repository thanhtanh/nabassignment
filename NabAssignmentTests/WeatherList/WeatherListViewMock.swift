//
//  WeatherListViewMock.swift
//  NabAssignmentTests
//
//  Created by Tanh Pham on 3/8/22.
//

import Foundation
@testable import NabAssignment

class WeatherListViewMock: WeatherListViewProtocol {
    private(set) var didShowTextSearchTooShort = false
    private(set) var didShowLoading = false
    private(set) var requestedToUpdateResult: WeatherListUpdateUIModel?
    private(set) var didShowFetchError = false
    
    var presenter: WeatherListPresenterProtocol?
    
    func updateWeatherList(items: [WeatherModel], city: String, country: String) {
        requestedToUpdateResult = WeatherListUpdateUIModel(city: city, country: country, items: items)
    }
    
    func showLoading() {
        didShowLoading = true
    }
    
    func showFetchingError(_ error: String) {
        didShowFetchError = true
    }
    
    func showSearchTextTooShortError() {
        didShowTextSearchTooShort = true
    }
}

struct WeatherListUpdateUIModel {
    let city: String
    let country: String
    let items: [WeatherModel]
}

enum NetworkError: Error {
    case notFound
    case unknown
}
