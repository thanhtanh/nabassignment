//
//  NabAssignmentTests.swift
//  NabAssignmentTests
//
//  Created by Tanh Pham on 3/4/22.
//

import XCTest
@testable import NabAssignment

class WeatherListViewModelTests: XCTestCase {
    var view: WeatherListViewMock!
    var presenter: WeatherListPresenter!
    var interactor: WeatherListInteractorMock!
    
    private let city = "Bern"
    private let country = "CH"
    private let weathers = [WeatherListResponse.WeatherByDate(date: Date(), temperature: .init(average: 1), pressure: 1, humidity: 1, weathers: [.init(description: "rainy", icon: "10d")])]
    
    override func setUpWithError() throws {
        view = WeatherListViewMock()
        presenter = WeatherListPresenter()
        interactor = WeatherListInteractorMock()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testShoudShowErrorWhenTestSearchTooShort() {
        presenter.search(text: "ab")
        XCTAssertTrue(view.didShowTextSearchTooShort)
    }
    
    func testShouldShowDataForHappyCase() {
        let response = WeatherListResponse(city: .init(name: city, countryCode: country), weatherList: weathers)
        let successResult = WeatherListResultModel(weatherResponse: response)

        interactor.configure(result: .success(successResult))
        
        XCTAssertNil(view.requestedToUpdateResult)
        
        presenter.search(text: "abc")
        
        let exp = expectation(description: "fetch data")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            guard let result = self.view.requestedToUpdateResult else {
                return
            }
            
            if result.city == self.city,
               result.country == self.country,
               result.items.count == self.weathers.count {
                exp.fulfill()
            }
        }
        wait(for: [exp], timeout: 0.2)
    }

    func testShouldShowErrorForErrorCase() {
        interactor.configure(result: .failed(NetworkError.notFound))
        
        presenter.search(text: "abc")
        
        let exp = expectation(description: "fetch data")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            if self.view.didShowFetchError {
                exp.fulfill()
            }
        }
        wait(for: [exp], timeout: 0.2)
    }

    func testShouldShowSpinnerWhenFetching() {
        let response = WeatherListResponse(city: .init(name: city, countryCode: country), weatherList: weathers)
        let successResult = WeatherListResultModel(weatherResponse: response)

        interactor.configure(result: .success(successResult))
        
        presenter.search(text: "Bern")
        XCTAssertTrue(view.didShowLoading)
    }
}
