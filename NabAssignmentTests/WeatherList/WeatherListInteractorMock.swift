//
//  WeatherListInteractorMock.swift
//  NabAssignmentTests
//
//  Created by Tanh Pham on 3/8/22.
//

import Foundation
@testable import NabAssignment

class WeatherListInteractorMock: WeatherListInteractorProtocol {
    private var fetchResult: WeatherListFetchResult?
    
    func configure(result: WeatherListFetchResult) {
        self.fetchResult = result
    }
    
    func fetchWeather(textSearch: String, numberOfDays: Int, unit: String,
                      completion: @escaping (Result<WeatherListResultModel, Error>) -> Void) {
        guard let mockResult = fetchResult else { fatalError("Have to configure the reuslt first") }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            switch mockResult {
            case let .success(mockModel):
                completion(.success(mockModel))
            case let .failed(error):
                completion(.failure(error))
            }
        }
    }
}

enum WeatherListFetchResult {
    case success(WeatherListResultModel)
    case failed(Error)
}
