# iOS NAB Assignment

Below content are information that I would like to provide about my implementation

## Instructions

In order to have a clear code history, I remove the `Pods` folder from git. So please execute the `pod install` command to install Cocoapods libraries before runing the project. The Cocoapods version I am using is `1.11.2`. 

## About the chosen 3rd libraries

In the `Podfile`, you can see that I added 7 libraries. Below is the explanation of the purpose:

*  `Alamofire`: This is a famous network framework which most apps are using (Swift app). I don't want to "reinvent the wheel" by writing my own network layer.
*  `Moya`: This is a middle layer between our code and `Alamofire`. It helps our network code clearer, and easier to test with the mocking feature.
*  `SDWebImage`: This is also a famous framework for caching images. I don't want to "reinvent the wheel" by writing my own image caching mechanism. This is used to cache/display the images for different kinds of weather.
* `SnapKit`: Since I don't like Storyboards because it has so many disadvantages, I prefer to build the UI by code. We have many choices here, but `SnapKit` is the library that is familiar with me the most
* `RealmSwift`: Since this assignment asks for caching for the request, but the `openweathermap` API doesn't support caching on their server, we have to build our own caching mechanism using a database. Realm is the database that I've chosen.
* `Resolver`: I used this to inject the database and the network instance in the app instead of using the static methods or Singleton. Then we can share this instance for all the interactors.
* `ViewStates`: This is my library which I wrote 4 years ago (pretty outdated 😅) as a Cocoapods framework. You can find it here https://github.com/thanhtanh/ViewStates. It helps us to handle the loading, error states easier. The theming customization is fully supported.

## Checklist of items which have been done:

I have done all 11 points from the requirement.

## About the architecture

The architecture which I'm choosing is VIPER. The advantages of this architecture are that we will have a clean structure and easy to write tests.

## About the UI

I perform the search when user presses on the Search button instead of when user is typing, because for this kind of app. It's a waste of resources if we perform the search when user is typing, because no one wants to see the weather result around the world while typing.
I added the table header to know which city/country is displaying for the weather. Because the API is so smart, for example, we search `saigon`, but it will show `Ho Chi Minh`, so it's better that we know the city name. But it might be redundant compared to the requirement.

## For the caching approach

To cache the response to the Realm database, I just build a simple object with 3 fields for the `search text`, `expired date` and the `json response`. When you see `json` in `database`, you can feel it's bad, but in this case, I think it's ok. Some reasons:

- One day, if we want to change the model to parse the json, then just clear the cached data and fetch again. Because it's not user data, so there is no loss at all
- We don't need to perform the search/query for the weather data, so don't need to make things complicated with a detailed structure when storing data to the database.

For the `life-time` of the cached data, I set it 60 seconds only for testing purpose. But in theory, we can set it for 24 hours (for example). The constant to change is `WEATHER_RESPONSE_LIFE_TIME` 
