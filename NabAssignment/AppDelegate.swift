//
//  AppDelegate.swift
//  NabAssignment
//
//  Created by Tanh Pham on 3/4/22.
//

import UIKit
import Resolver
import RealmSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        let nav = UINavigationController(rootViewController: WeatherList.createModule())
        window?.rootViewController = nav
        window?.makeKeyAndVisible()
        
        configureDatabase()
        registerDependencies()
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    private func configureDatabase() {
        Realm.Configuration.defaultConfiguration = RealmStorageContext.configuration
    }

    private func registerDependencies() {
        Resolver.register { Network() }
        Resolver.register { RealmStorageContext() as StorageContext }
    }
}

