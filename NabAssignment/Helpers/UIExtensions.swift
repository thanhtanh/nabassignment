//
//  UIExtensions.swift
//  NabAssignment
//
//  Created by Tanh Pham on 3/4/22.
//

import UIKit

extension UITableViewCell {
    static var reusedIdentifier: String {
        return "\(self)ReuseIdentifier"
    }
}

extension UITableViewHeaderFooterView {
    static var reusedIdentifier: String {
        return "\(self)ReuseIdentifier"
    }
}

extension UIView {
    func addSubviews(_ views: UIView...) {
        views.forEach { addSubview($0) }
    }
}

extension UIColor {
    convenience init(hex: String) {
        let r, g, b, a: CGFloat
        
        var hexColor = hex
        if hexColor.hasPrefix("#") {
            hexColor.removeFirst()
        }
        
        if hexColor.count == 6 {
            hexColor += "FF" //append alpha
        }
            
        if hexColor.count == 8 {
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            
            if scanner.scanHexInt64(&hexNumber) {
                r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                a = CGFloat(hexNumber & 0x000000ff) / 255
                
                self.init(red: r, green: g, blue: b, alpha: a)
                return
            }
        }
        
        fatalError("Color code is not valid")
    }
}
