//
//  GenericExtensions.swift
//  NabAssignment
//
//  Created by Tanh Pham on 3/4/22.
//

import Foundation

precedencegroup ForwardPipeline {
    higherThan: AssignmentPrecedence
    associativity: left
}

infix operator ||> : ForwardPipeline

public func ||><A> ( rhs: A, lhs: (A) -> Void) -> A {
    lhs(rhs)
    return rhs
}

extension DateFormatter {
    static let appDisplayDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_GB")
        formatter.dateFormat = "EEE, dd MMM yyyy"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        
        return formatter
    }()
}

extension Encodable {
    var jsonString: String? {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        do {
            let jsonData = try jsonEncoder.encode(self)
            return String(data: jsonData, encoding: .utf8)
        } catch {
            return nil
        }
    }
}
