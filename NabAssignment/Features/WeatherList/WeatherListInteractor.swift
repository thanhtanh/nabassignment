//
//  WeatherListInteractor.swift
//  NabAssignment
//
//  Created by Tanh Pham on 3/4/22.
//

import Foundation
import Resolver
import RealmSwift

protocol WeatherListInteractorProtocol {
    func fetchWeather(textSearch: String, numberOfDays: Int, unit: String,
                      completion: @escaping (Result<WeatherListResultModel, Error>) -> Void)
}

class WeatherListInteractor: WeatherListInteractorProtocol {
    @LazyInjected
    private var network: Network
    
    @LazyInjected
    private var database: StorageContext
    
    private let WEATHER_RESPONSE_LIFE_TIME = 60
    
    func fetchWeather(textSearch: String, numberOfDays: Int, unit: String,
                      completion: @escaping (Result<WeatherListResultModel, Error>) -> Void) {
        if let savedResponse = fetchCachedWeatherData(textSearch: textSearch) {
            let result = WeatherListResultModel(weatherResponse: savedResponse)
            return completion(.success(result))
        }
        
        let target = OpenWeatherService.searchByCity(textSearch: textSearch,
                                                     numberOfDays: numberOfDays,
                                                     unit: unit)
        network.request(target: target) { [weak self] (result: Result<WeatherListResponse, Error>) in
            guard let self = self else { return }
            switch result {
            case let .success(response):
                self.saveWeatherResponse(searchText: textSearch, response: response)
                let result = WeatherListResultModel(weatherResponse: response)
                completion(.success(result))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
    
    private func fetchCachedWeatherData(textSearch: String) -> WeatherListResponse? {
        let predicate = NSPredicate(format: "searchText == %@", textSearch)
        let results = database.fetch(CachedWeatherResponse.self, predicate: predicate)
        guard let found = results.first,
              let expiredDate = found.expiredDate,
              expiredDate > Date(),
              let json = found.jsonString,
              let data = json.data(using: .utf8),
              let response = try? JSONDecoder().decode(WeatherListResponse.self, from: data) else  {
                  return nil
              }
        
        return response
    }
    
    private func saveWeatherResponse(searchText: String, response: WeatherListResponse) {
        let dbModel = CachedWeatherResponse()
        dbModel.searchText = searchText
        dbModel.jsonString = response.jsonString
        
        let expiredDate = Calendar.current.date(byAdding: .second, value: WEATHER_RESPONSE_LIFE_TIME, to: Date())
        dbModel.expiredDate = expiredDate
        
        do {
            try database.save(object: dbModel)
        } catch {
            print(error.localizedDescription)
        }
    }
}

struct WeatherListResponse: Codable {
    let city: City
    let weatherList: [WeatherByDate]
    
    enum CodingKeys: String, CodingKey {
        case city
        case weatherList = "list"
    }
}

extension WeatherListResponse {
    struct City: Codable {
        let name: String
        let countryCode: String
        
        enum CodingKeys: String, CodingKey {
            case name
            case countryCode = "country"
        }
    }
    
    struct WeatherByDate: Codable {
        let date: Date
        let temperature: Temperature
        let pressure: Int
        let humidity: Int
        let weathers: [Weather]
        
        enum CodingKeys: String, CodingKey {
            case date = "dt"
            case temperature = "temp"
            case pressure
            case humidity
            case weathers = "weather"
        }
    }
    
    struct Weather: Codable {
        let description: String
        let icon: String
    }
    
    struct Temperature: Codable {
        let average: Double
        
        enum CodingKeys: String, CodingKey {
            case average = "day"
        }
    }
}

class CachedWeatherResponse: Object {
    @objc dynamic var searchText: String?
    @objc dynamic var jsonString: String?
    @objc dynamic var expiredDate: Date?
    
    override class func primaryKey() -> String? {
        return "searchText"
    }
}
