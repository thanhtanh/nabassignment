//
//  WeatherListPresenter.swift
//  NabAssignment
//
//  Created by Tanh Pham on 3/4/22.
//

import Foundation

protocol WeatherListPresenterProtocol {
    var view: WeatherListViewProtocol? { get }
    var interactor: WeatherListInteractorProtocol? { get }
//    var router: MoviesListRouterProtocol? { get }
    
    func search(text: String)
}

class WeatherListPresenter: WeatherListPresenterProtocol {
    weak var view: WeatherListViewProtocol?
    var interactor: WeatherListInteractorProtocol?
    
    private let SEARCH_TEXT_MIN_LENGTH = 3
    
    func search(text: String) {
        let trimmedText = text.trimmingCharacters(in: .whitespacesAndNewlines)
        guard trimmedText.count >= SEARCH_TEXT_MIN_LENGTH else {
            view?.showSearchTextTooShortError()
            return
        }
        
        view?.showLoading()
        interactor?.fetchWeather(textSearch: trimmedText, numberOfDays: 7, unit: "metric",
                                 completion: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(model):
                self.view?.updateWeatherList(items: model.weatherData, city: model.city, country: model.countryCode)
            case let .failure(error):
                if let networkError = error as? NetworkError,
                   networkError == .notFound {
                    self.view?.showFetchingError("No result for this search")
                } else {
                    self.view?.showFetchingError(error.localizedDescription)
                }
            }
        })
    }
}
