//
//  WeatherListModels.swift
//  NabAssignment
//
//  Created by Tanh Pham on 3/5/22.
//

import Foundation

private let IMAGE_SERVER_BASE_URL = "https://openweathermap.org/img/w/"

struct WeatherListResultModel {
    let city: String
    let countryCode: String
    
    let weatherData: [WeatherModel]
    
    init(weatherResponse: WeatherListResponse) {
        self.city = weatherResponse.city.name
        self.countryCode = weatherResponse.city.countryCode
        self.weatherData = weatherResponse.weatherList.map { WeatherModel(weather: $0, city: weatherResponse.city.name) }
    }
}

struct WeatherModel: Hashable {
    let city: String
    let date: String
    let evarageTemperature: Double
    let pressure: Int
    let humidity: Int
    let weatherDescription: String
    let weatherIconUrl: String?
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(date)
        hasher.combine(city)
    }
    
    static func == (lhs: WeatherModel, rhs: WeatherModel) -> Bool {
        return lhs.date == rhs.date &&
        lhs.city == rhs.city
    }
    
    init(weather: WeatherListResponse.WeatherByDate, city: String) {
        self.city = city
        self.date = DateFormatter.appDisplayDateFormatter.string(from: weather.date)
        self.evarageTemperature = weather.temperature.average
        self.pressure = weather.pressure
        self.humidity = weather.humidity
        self.weatherDescription = weather.weathers.first?.description ?? ""
        if let icon = weather.weathers.first?.icon {
            self.weatherIconUrl = IMAGE_SERVER_BASE_URL + icon + ".png"
        } else {
            self.weatherIconUrl = nil
        }
    }
}
