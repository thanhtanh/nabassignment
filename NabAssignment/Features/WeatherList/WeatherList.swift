//
//  WeatherList.swift
//  NabAssignment
//
//  Created by Tanh Pham on 3/5/22.
//

import Foundation

struct WeatherList {
    static func createModule() -> WeatherListViewController {
        let view = WeatherListViewController()
        let presenter = WeatherListPresenter()
        let interactor = WeatherListInteractor()
//        let router = MoviesListRouter()
        
        view.presenter = presenter
        presenter.view = view
//        presenter.router = router
        presenter.interactor = interactor
//        
//        router.viewController = view
        
        return view
    }
}
