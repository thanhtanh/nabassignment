//
//  WeatherListViewController.swift
//  NabAssignment
//
//  Created by Tanh Pham on 3/4/22.
//

import Foundation
import UIKit
import ViewStates

typealias TableDataSource = UITableViewDiffableDataSource<Int, WeatherModel>

protocol WeatherListViewProtocol: AnyObject {
    var presenter: WeatherListPresenterProtocol? { get }
    
    func updateWeatherList(items: [WeatherModel], city: String, country: String)
    func showLoading()
    func showFetchingError(_ error: String)
    func showSearchTextTooShortError()
}

class WeatherListViewController: UIViewController {
    var presenter: WeatherListPresenterProtocol?
    
    private lazy var tableView = UITableView() ||> {
        $0.tableFooterView = UIView()
        $0.backgroundColor = .white
        $0.register(WeatherItemCell.self, forCellReuseIdentifier: WeatherItemCell.reusedIdentifier)
        $0.register(WeatherListHeaderView.self, forHeaderFooterViewReuseIdentifier: WeatherListHeaderView.reusedIdentifier)
        $0.delegate = self
    }
    
    private lazy var searchBar = UISearchBar() ||> {
        $0.delegate = self
        $0.placeholder = "Search weather by city..."
        $0.accessibilityLabel = $0.placeholder
    }
    
    private lazy var datasource: TableDataSource = {
        let datasource = TableDataSource(tableView: tableView, cellProvider: { (tableView, indexPath, model) -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: WeatherItemCell.reusedIdentifier,
                                                     for: indexPath) as? WeatherItemCell
            cell?.configure(model: model)
            return cell
        })
        
        return datasource
    }()
    
    private lazy var viewState = ViewState()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buildUI()
    }
    
    private func buildUI() {
        title = "Weather Forecast"
        view.backgroundColor = .white
        
        view.addSubviews(tableView, searchBar)
        
        searchBar.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.leading.trailing.equalToSuperview()
        }
        
        tableView.snp.makeConstraints { make in
            make.top.equalTo(searchBar.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
        
        tableView.dataSource = datasource
        viewState.parentView = tableView
        viewState.hideViewState()
    }
}

extension WeatherListViewController: WeatherListViewProtocol {
    func updateWeatherList(items: [WeatherModel], city: String, country: String) {
        var snapshot = NSDiffableDataSourceSnapshot<Int, WeatherModel>()
        snapshot.appendSections([0])
        snapshot.appendItems(items, toSection: 0)
        datasource.apply(snapshot) { [weak self] in
            self?.updateLocation(city: city, country: country)
        }
        
        viewState.hideViewState()
        searchBar.resignFirstResponder()
    }
    
    private func updateLocation(city: String, country: String) {
        guard let header = tableView.headerView(forSection: 0) as? WeatherListHeaderView else { return }
        header.configure(city: city, country: country)
    }
    
    func showLoading() {
        viewState.showLoadingState()
    }
    
    func showFetchingError(_ error: String) {
        viewState.showErrorState(error)
        
        let snapshot = NSDiffableDataSourceSnapshot<Int, WeatherModel>()
        datasource.apply(snapshot)
    }
    
    func showSearchTextTooShortError() {
        // I know this has a bad UX, but in the real life, we should add the toast message instead of an alert
        let alert = UIAlertController(title: "Error", message: "Please enter at least 3 characters to search",
                                      preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}

extension WeatherListViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.searchBar.showsCancelButton = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchText = searchBar.text ?? ""
        presenter?.search(text: searchText)
    }
}

extension WeatherListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: WeatherListHeaderView.reusedIdentifier)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52
    }
}
