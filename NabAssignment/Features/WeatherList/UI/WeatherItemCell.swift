//
//  WeatherItemCell.swift
//  NabAssignment
//
//  Created by Tanh Pham on 3/4/22.
//

import Foundation
import UIKit
import SnapKit
import SDWebImage

class WeatherItemCell: UITableViewCell {
    private lazy var stackView = UIStackView() ||> {
        $0.axis = .vertical
        $0.spacing = 8
    }
    
    private lazy var iconImageView = UIImageView() ||> {
        $0.setContentHuggingPriority(.required, for: .horizontal)
    }
    
    private lazy var dateLabel = ScalableLabel()
    private lazy var temperatureLabel = ScalableLabel()
    private lazy var pressureLabel = ScalableLabel()
    private lazy var huminityLabel = ScalableLabel()
    private lazy var descriptionLabel = ScalableLabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        selectionStyle = .none
        contentView.addSubviews(stackView, iconImageView)
        
        stackView.snp.makeConstraints { make in
            make.leading.top.bottom.equalToSuperview().inset(16)
        }
        
        iconImageView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(16)
            make.centerY.equalToSuperview()
            make.leading.equalTo(stackView.snp.trailing).offset(16)
        }
        
        [
            dateLabel,
            temperatureLabel,
            pressureLabel,
            huminityLabel,
            descriptionLabel
        ].forEach { stackView.addArrangedSubview($0) }
    }
    
    func configure(model: WeatherModel) {
        dateLabel.text = "Date: " + model.date
        temperatureLabel.text = "Evarage Temperature: \(model.evarageTemperature)°C"
        pressureLabel.text = "Pressure: \(model.pressure)"
        huminityLabel.text = "Huminity: \(model.humidity)%"
        descriptionLabel.text = "Description: \(model.weatherDescription)"
        
        iconImageView.sd_setImage(with: URL(string: model.weatherIconUrl ?? ""))
        
        dateLabel.accessibilityLabel = dateLabel.text
        temperatureLabel.accessibilityLabel = temperatureLabel.text
        pressureLabel.accessibilityLabel = pressureLabel.text
        huminityLabel.accessibilityLabel = huminityLabel.text
        descriptionLabel.accessibilityLabel = descriptionLabel.text
    }
}

private class ScalableLabel: UILabel {
    init() {
        super.init(frame: .zero)
        font = .preferredFont(forTextStyle: .body)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
