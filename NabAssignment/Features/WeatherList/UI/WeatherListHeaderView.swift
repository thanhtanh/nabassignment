//
//  WeatherListHeaderView.swift
//  NabAssignment
//
//  Created by Tanh Pham on 3/6/22.
//

import UIKit

class WeatherListHeaderView: UITableViewHeaderFooterView {
    private lazy var cityLabel = UILabel() ||> {
        $0.font = .preferredFont(forTextStyle: .headline)
    }
    
    private lazy var countryLabel = UILabel() ||> {
        $0.font = .preferredFont(forTextStyle: .headline)
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildUI() {
        tintColor = .systemGray5
        contentView.addSubviews(cityLabel, countryLabel)
        
        cityLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(16)
            make.centerY.equalToSuperview()
        }
        
        countryLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(16)
            make.centerY.equalToSuperview()
        }
    }
    
    func configure(city: String, country: String) {
        cityLabel.text = "City: \(city)"
        countryLabel.text = "Country: \(country)"
        
        cityLabel.accessibilityLabel = cityLabel.text
        countryLabel.accessibilityLabel = countryLabel.text
    }
}
