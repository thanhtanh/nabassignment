//
//  StorageContext.swift
//  Anibis
//
//  Created by Tanh Pham on 9/12/19.
//  Copyright © 2019 Xmedia AG. All rights reserved.
//

import Foundation
import RealmSwift

struct Sorted {
    var key: String
    var ascending: Bool = true
}

public protocol Storable { }

extension Object: Storable { }

/*
 Operations on context
 */
protocol StorageContext {
    /*
     Save an object that is conformed to the `Storable` protocol
     */
    func save(object: Storable) throws
    /*
     Update an object that is conformed to the `Storable` protocol
     */
    func update(block: @escaping () -> Void) throws
    /*
     Delete an object that is conformed to the `Storable` protocol
     */
    func delete(object: Storable) throws
    /*
     Delete all objects that are conformed to the `Storable` protocol
     */
    func deleteAll<T: Storable>(_ model: T.Type) throws
    /*
     Return a list of objects that are conformed to the `Storable` protocol
     */
    func fetch<T: Storable>(_ model: T.Type, predicate: NSPredicate?, sorted: Sorted?) -> [T]
    
    /*
     Return an object with the give id. The primary key is get automatically
    */
    func fetchById<T: Storable>(_ model: T.Type, id: Int) -> T?
}

extension StorageContext {
    /*
     Return a list of objects that are conformed to the `Storable` protocol with some default values
     https://medium.com/@georgetsifrikas/swift-protocols-with-default-values-b7278d3eef22
     */
    func fetch<T: Storable>(_ model: T.Type, predicate: NSPredicate? = nil, sorted: Sorted? = nil) -> [T] {
        return fetch(model, predicate: predicate, sorted: sorted)
    }
}
