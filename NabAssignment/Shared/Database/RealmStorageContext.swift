//
//  RealmStorageContext.swift
//  Anibis
//
//  Created by Tanh Pham on 9/12/19.
//  Copyright © 2019 Xmedia AG. All rights reserved.
//

import Foundation
import RealmSwift

private let CURRENT_SCHEMA_VERSION: UInt64 = 1

class RealmStorageContext: StorageContext {
    static let configuration = Realm.Configuration(schemaVersion: CURRENT_SCHEMA_VERSION, migrationBlock: { (migration, oldSchemaVersion) in
        // Perform migration for version 0 -> 1
    })
    
    private let realm: Realm
    
    required init() {
        do {
            try self.realm = Realm()
        } catch {
            fatalError("Could not initalize the REALM database")
        }
    }
    
    func safeWrite(_ block: (() throws -> Void)) throws {
        if realm.isInWriteTransaction {
            try block()
        } else {
            try realm.write(block)
        }
    }
}

extension RealmStorageContext {    
    func save(object: Storable) throws {
        guard let object = object as? Object else {
            fatalError("REALM ERROR: The model should be an Object type")
        }
        
        try self.safeWrite {
            if type(of: object).primaryKey() != nil {
                realm.add(object, update: .modified)
            } else {
                realm.add(object, update: .error)
            }
        }
    }
    
    func update(block: @escaping () -> Void) throws {
        try self.safeWrite {
            block()
        }
    }
}

extension RealmStorageContext {
    func delete(object: Storable) throws {
        guard let object = object as? Object else {
            fatalError("REALM ERROR: The model should be an Object type")
        }
        
        try self.safeWrite {
            realm.delete(object)
        }
    }
    
    func deleteAll<T: Storable>(_ model: T.Type) throws {
        guard let type = model as? Object.Type else {
            fatalError("REALM ERROR: The model should be an Object type")
        }
        
        try self.safeWrite {
            let objects = realm.objects(type)
            
            for object in objects {
                realm.delete(object)
            }
        }
    }
    
    func reset() throws {
        try self.safeWrite {
            realm.deleteAll()
        }
    }
}

extension RealmStorageContext {
    func fetch<T: Storable>(_ model: T.Type, predicate: NSPredicate? = nil, sorted: Sorted? = nil) -> [T] {
        guard let type = model as? Object.Type else {
            fatalError("REALM ERROR: The model should be an Object type")
        }
        
        var objects = self.realm.objects(type)
        
        if let predicate = predicate {
            objects = objects.filter(predicate)
        }
        
        if let sorted = sorted {
            objects = objects.sorted(byKeyPath: sorted.key, ascending: sorted.ascending)
        }
        
        var accumulate: [T] = [T]()
        for object in objects {
            if let object = object as? T {
                accumulate.append(object)
            }
        }
        
        return accumulate
    }
    
    func fetchById<T: Storable>(_ model: T.Type, id: Int) -> T? {
        guard let type = model as? Object.Type else {
            fatalError("REALM ERROR: The model should be an Object type")
        }
        
        if let primaryKey = type.primaryKey() {
            let predicate = NSPredicate(format: "\(primaryKey) = %d", id)
            return fetch(type, predicate: predicate, sorted: nil).first as? T
        }
        return nil
    }
}
