//
//  OpenWeatherService.swift
//  NabAssignment
//
//  Created by Tanh Pham on 3/5/22.
//

import Foundation
import Moya

private let APP_ID = "60c6fbeb4b93ac653c492ba806fc346d"

enum OpenWeatherService {
    case searchByCity(textSearch: String, numberOfDays: Int, unit: String)
}

extension OpenWeatherService: TargetType {
    var baseURL: URL {
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/") else {
            fatalError("Base url is not valid")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .searchByCity:
            return "forecast/daily"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        var params: [String: Any] = [
            "appid": APP_ID
        ]
        
        switch self {
        case let .searchByCity(textSearch, numberOfDays, unit):
            params["q"] = textSearch
            params["cnt"] = numberOfDays
            params["units"] = unit
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
}
